<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi', function (Blueprint $table) {
            $table->integer('id_transaksi')->unsigned();
            $table->integer('id_barang')->unsigned();
            $table->integer('jumlah');
            $table->double('harga');
            $table->timestamps();
            $table->softDeletes();
            $table->primary(['id_transaksi', 'id_barang']);

            $table->foreign('id_transaksi')
            ->references('id_transaksi')
            ->on('transaksi')
        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi');
    }
}
