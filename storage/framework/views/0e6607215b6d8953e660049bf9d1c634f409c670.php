<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login Aplikasi Pemasaran Eastmovin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->  
    <link rel="icon" type="image/png" href="<?php echo e(url('log/images/icons/favicon.ico')); ?>"/>
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/vendor/bootstrap/css/bootstrap.min.css')); ?>">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/fonts/font-awesome-4.7.0/css/font-awesome.min.css')); ?>">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')); ?>">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/vendor/animate/animate.css')); ?>">
<!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/vendor/css-hamburgers/hamburgers.min.css')); ?>">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/vendor/select2/select2.min.css')); ?>">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/css/util.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('log/css/main.css')); ?>">
<!--===============================================================================================-->
</head>
<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url('log/images/img-01.jpg');">
            <div class="wrap-login100 p-b-30">
                <form method="POST" action="<?php echo e(route('login')); ?>" autocomplete="off">
                        <?php echo csrf_field(); ?>
                    <div class="login100-form-avatar">
                        <img src="<?php echo e(url('log/images/avatar-01.jpg')); ?>" alt="AVATAR">
                    </div>

                    <span class="login100-form-title p-t-20 p-b-45">
                        Aplikasi Pemasaran Eastmovin
                    </span>

                    <div class="wrap-input100 validate-input m-b-10" data-validate = "Username is required">
                        <input class="input100<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" type="text" name="email" placeholder="Username" value="<?php echo e(old('username')); ?>">
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-users"></i>
                        </span>
                    </div>
                    <?php if($errors->has('email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                <?php endif; ?>

                    <div class="wrap-input100 validate-input m-b-10" data-validate = "Password is required">
                        <input class="input100<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" type="password" name="password" placeholder="Password" required>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock"></i>
                        </span>
                    </div>
                    <?php if($errors->has('password')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('password')); ?></strong>
                                    </span>
                                <?php endif; ?>

                    <div class="container-login100-form-btn p-t-10">
                        <button type="submit" class="login100-form-btn">
                            <?php echo e(__('Login')); ?>

                        </button>
                    </div>

                    <!-- <div class="text-center w-full">
                        <a class="txt1" href="<?php echo e(route('register')); ?>">
                            Create new account
                            <i class="fa fa-long-arrow-right"></i>                      
                        </a>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
<!--===============================================================================================-->  
    <script src="<?php echo e(url('log/vendor/jquery/jquery-3.2.1.min.js')); ?>"></script>
<!--===============================================================================================-->
    <script src="<?php echo e(url('log/vendor/bootstrap/js/popper.js')); ?>"></script>
    <script src="<?php echo e(url('log/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
<!--===============================================================================================-->
    <script src="<?php echo e(url('log/vendor/select2/select2.min.js')); ?>"></script>
<!--===============================================================================================-->
    <script src="<?php echo e(url('log/js/main.js')); ?>"></script>

</body>
</html>