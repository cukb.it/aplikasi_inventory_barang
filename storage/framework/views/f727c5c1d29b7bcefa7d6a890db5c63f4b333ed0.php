<?php $__env->startSection('title','Data User'); ?>
<?php $__env->startSection('masteruser'); ?>
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
		<?php if($message = Session::get('success')): ?>
	<div class="alert alert-success">
		<p><?php echo e($message); ?></p>
	</div>
	<?php endif; ?>
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Data User
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="<?php echo e(url('master.user.form')); ?>" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
					<i class="la la-plus"></i>
					<span>Tambah User</span>
				</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
			<thead >
				<tr class="text-center">
					<th width="5%">No</th>
					<th>Username</th>
					<th>Email</th>
					<th>Role</th>
					<th>Status</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$i); ?>.</td>
					<td><?php echo e($user->username); ?></td>
					<td><?php echo e($user->email); ?></td>
					<td class="text-center"><?php echo e($user->role); ?></td>					
				<td class="text-center"><?php echo e($user->status); ?></td>
					<td class="text-center">
					<a href="master.user.<?php echo e($user->id_user); ?>.edit_user" class="btn btn-outline-warning m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air"><i class="fa fa-user-edit"></i></a>

					<a href="master/user/<?php echo e($user->id_user); ?>/destroy" class="btn btn-outline-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"><i class="fa fa-trash"></i></a>
				</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.masteruser', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>