<table border="2px">
<tr>
	<td colspan="5">Nama Barang : <?php echo e($deta->nama_barang); ?></td>
</tr>
<tr>
	<td colspan="4">Periode : <?php echo e($awal); ?> s/d <?php echo e($akhir); ?></td>
</tr>
<tr></tr>
	<tr>
		<td rowspan="2">No.</td>
		<td rowspan="2">Tanggal</td>
		<td colspan="2">Jumlah</td>
	</tr>
	<tr>
		<td>Masuk</td>
		<td>Keluar</td>
	</tr>
	<?php $__currentLoopData = $dets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $det): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<tr>
		<td><?php echo e(++$i); ?></td>
		<td><?php echo e($det->tanggal); ?></td>

		<?php if($det->jenis == 1): ?>
		<td><?php echo e($det->jumlah); ?></td>
		<td></td>
		<?php elseif($det->jenis == 0): ?>
		<td></td>
		<td><?php echo e($det->jumlah); ?></td>
		<?php endif; ?>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<tr>
		<td></td>
		<td>Total</td>
		<td>=SUM(C6:C<?php echo e($i+5); ?>)</td>
		<td>=SUM(D6:D<?php echo e($i+5); ?>)</td>
	</tr>
	<tr>
		<td></td>
		<td>Sisa</td>
		<td colspan="2">=C<?php echo e($i+6); ?>-D<?php echo e($i+6); ?></td>
	</tr>
</table>