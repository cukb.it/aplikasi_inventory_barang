<?php

namespace App\Exports;

use App\transaksi;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\excelformat\number;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Carbon\Carbon;
class detailExport implements FromView,ShouldAutoSize,WithColumnFormatting
{

	public function __construct(string $tanggal,string $tanggal_akhir,int $jenis)
	{
		$this->jenis = $jenis;
		$this->tanggal = Carbon::parse($tanggal)->formatLocalized('%Y-%m-%d');
		$this->tanggal_akhir = Carbon::parse($tanggal_akhir)->formatLocalized('%Y-%m-%d');
		
	}

	public function view(): View
    {
        return view('tampilan', [
            'dets' => transaksi::first()->join('detail_transaksi','detail_transaksi.id_transaksi','=','transaksi.id_transaksi')
		->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
		->where('tanggal','>=',$this->tanggal)
		->Where('tanggal','<=', $this->tanggal_akhir)
		->where('jenis','=', $this->jenis)
		->get()
		->sortBy('tanggal')
        ])->with('i');
    }
    public function columnFormats(): array
    {
        return [
            'F' => number::FORMAT_CURRENCY_IDR_SIMPLE,
        ];
    }
}

