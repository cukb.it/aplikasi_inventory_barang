<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Hash;
use Validator;
use Alert;
class PasswordController extends Controller
{
     /**
     * @return mixed
     */
     public function change()
     {
        return view('auth.passwords.change');
    }

    /**
     * @return mixed Redirect
     */
    public function update(Request $request)
    {
        // custom validator
        Validator::extend('password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, \Auth::user()->password);
        });

        // message for custom validation
        $messages = [
            'password' => 'Invalid current password.',
        ];

        // validate form
        $validator = Validator::make(request()->all(), [
            'current_password'      => 'required|password',
            'password'              => 'required|min:5|confirmed',
            'password_confirmation' => 'required',

        ], $messages);

         if(strlen($request->get('password')) == ''){
            Alert::error('Whoops! Inputkankan Data Dengan Benar!!','Error')->persistent("OK");
            return Redirect('password.change');
        }else if(strlen($request->get('password')) <= 4){
            Alert::error('Password terlalu pendek!! inputkan minimal 5 karakter!','Failed')->persistent("OK");
            return Redirect('password.change');
        }

        // update password
        $user = User::find(Auth::id());

        $user->password = bcrypt(request('password'));
        $user->save();

        return redirect()
        ->route('password.change')
        ->withSuccess('Password has been updated.');
    }


}
