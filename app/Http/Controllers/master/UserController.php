<?php

namespace App\Http\Controllers\master;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pengguna;
use Illuminate\Support\Facades\DB;
use Alert;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
            /**
     * Create a new controller instance.
     *
     * @return void
     */
            public function __construct()
            {
                $this->middleware('auth');
            }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $b = 1;
        $d = 0;
        $users = Pengguna::latest()->paginate();
        $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
        if ($akses->role == 1) {
        return view('master/user/grid', compact('users','b','d'))
        ->with('i',(request()->input('page',1) -1)*5);
        }else{
            Alert::warning('Anda Login Sebagai User','Halaman Tidak Bisa Diakses')->persistent("OK");
            return redirect('/dasboard.aplikasi');
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
        if ($akses->role == 1) {
        return view('master.user.form');
        }else{
            Alert::warning('Anda Login Sebagai User','Halaman Tidak Bisa Diakses')->persistent("OK");
            return redirect('/dasboard.aplikasi');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required',
            'role' => 'required',
            'status' => 'required'
        ]);
        $do = Pengguna::create([
            'username' => $request->username,
            'email' => $request->email,
            'role' => $request->role,
            'status' => $request->status
        ])->id_user;
        
        User::create([
            'id' => $do,
            'name' => request('name'),
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'remember_token' => str_random(60),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);

        Alert::success('Data Yang Anda Inputkan Berhasil Di Tambahkan','Tersimpan')->persistent("OK");
        return redirect('master.user.grid');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id_user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id_user)
    {
        $akses = DB::table('user')
        ->join('users','users.id','=','user.id_user')
        ->where('user.username','=',Auth::user()->username)
        ->first();
         if ($akses->role == 1) {
        $users = Pengguna::find($id_user);   
        return view('master.user.edit_user',compact('users'));
        }else{
            Alert::warning('Anda Login Sebagai User','Halaman Tidak Bisa Diakses')->persistent("OK");
            return redirect('/dasboard.aplikasi');
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_user)
    {
     $this->validate($request, [
        'username' => 'required',
        'email' => 'required',
        'role' => 'required',
        'status' => 'required'
    ]);
     $users = Pengguna::find($id_user);
     $users->username = $request->get('username');
     $users->email = $request->get('email');
     $users->role = $request->get('role');
     $users->status = $request->get('status');
     $users->save();

     $akun = User::find($id_user);
     $akun->name = $request->get('name');
     $akun->username = $request->get('username');
     $akun->save();
      Alert::success('Data Berhasil Di Update','Tersimpan')->persistent("OK");
     return redirect('master.user.grid');
 }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_user)
    {
        $user = Pengguna::find($id_user);
        $user->delete();
        $users = User::find($id_user);
        $users->delete();
        return redirect('master.user.grid')
        ->with('success','Data berhasil dihapus');
    }

    public function reset($id_user)
    {
        $akun = User::find($id_user);
        $akun->password = bcrypt('energeek');
        $akun->save();
        Alert::success('Password Berhasil Direset','Sukses')->persistent("OK");
        return redirect('master.user.grid');

    }
}
