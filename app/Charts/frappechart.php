<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\Frappe\Chart;

class frappechart extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}
