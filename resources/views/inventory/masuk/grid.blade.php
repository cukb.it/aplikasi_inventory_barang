@extends('layouts.inventorymasuk')
@section('title','Inventory Barang Masuk')
@section('masuk')
<div class="m-portlet m-portlet--mobile" style="width: 100%;">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Inventory Barang Masuk
				</h3>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{url('inventory.masuk.form')}}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
							<span>Tambah Data</span>
						</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="m-portlet__body">

		<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_masuk">
			<thead >
				<tr class="text-center">
					<th width="20px">No.</th>
					<th width="20%">Tanggal</th>
					<th width="40%">Keterangan</th>
					<th width="20%">Hak Akses</th>
					<th width="40%">Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach($transaksi as $trans)
				<tr>
					<td>{{++$i}}.</td>
					<td>{{ Carbon\Carbon::parse($trans->tanggal)->formatLocalized('%A, %d %B %Y')}}</td>
					<td>{{$trans->keterangan}}</td>
					<td>{{$trans->permission}}</td>
					<td class="text-center">
						@if ($trans->permission == 'terkunci')
							<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.masuk.{{$trans->id_transaksi}}.show" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "1000px"}}}'><i class="fas fa-id-card"></i></a>
							<button class="btn btn-danger active m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-lock"></i>
							</button>
						@elseif ($trans->permission == 'terbuka')
						<form action="inventory/masuk/{{$trans->id_transaksi}}/permission" method="post" enctype="multipart/form-data" class="test{{$i}}">
							@csrf
							
							<input type="hidden" name="permission" value="terkunci">
							<?php 
							$dets = DB::table('detail_transaksi')
							->join('barang','barang.id_barang','=','detail_transaksi.id_barang')
							->where('id_transaksi','=', $trans->id_transaksi)->paginate();
							?>
							<table style="display: none;">
								@foreach($dets as $deta)
								<tr>
									<td>
										stok <input style="width: 100%;" type="text" name="stok[]" readonly class="form-control mb-2" value="{{$deta->stok}}">
									</td>
									<td>
										jumlah <input style="width: 100%;" type="text" name="jumlah[]" class="form-control mb-2" placeholder="Jumlah" autocomplete="off" value="{{$deta->jumlah}}">
									</td>
									<td>
										id_barang <input type="text" name="id_barang[]" readonly class="form-control mb-2" value="{{$deta->id_barang}}" >
									</td>
								</tr>
								@endforeach
							</table>
							
							<a class="btn btn-outline-info m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.masuk.{{$trans->id_transaksi}}.show" data-fancybox data-options='{"type" : "iframe", "iframe" : {"preload" : false, "css" : {"width" : "1000px"}}}'><i class="fas fa-id-card"></i></a>

							<a class="btn btn-outline-warning m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air" href="inventory.masuk.{{$trans->id_transaksi}}.edit"><i class="fas fa-user-edit"></i></a>

							<a href="inventory/masuk/{{$trans->id_transaksi}}/destroy" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air"><i class="fas fa-trash-alt" onclick="return confirm('Apakah Anda Yakin Untuk Menghapus')"></i></a>

							<a href="#" class="btn btn-outline-primary m-btn m-btn--icon m-btn--icon-only m-btn--pill m-btn--air kunci{{$i}}"><i class="fas fa-lock"></i>
							</a>
						</form>
						@else
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){ 
	<?php foreach ($transaksi as $trans): ?>
		var q{{++$d}} = {{$b++}};
	 	$('.kunci'+q{{$d}}+'').click(function(){  
			Swal.fire({
				title: 'Apakah Anda Yakin Untuk Mengunci Data Ini?',
				text: "Data Yang Sudah Dikunci Tidak Dapat Dibuka!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, Kunci Data Ini!',
				cancelButtonText: 'Tidak, Cancel!',
			}).then((result) => {
				if (result.value) {
					$('.test'+q{{$d}}+'').submit();
				}
			});
		});
	 <?php endforeach ?> 
		
	});
</script>
@endsection