<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('auth.login');
});
Auth::routes();
Route::group(['middleware' => 'revalidate'], function()
{
Route::group(['middleware' => 'auth'], function () {
	Route::get('password.change', 'Auth\PasswordController@change')->name('password.change');
	Route::put('password.update', 'Auth\PasswordController@update')->name('password.update');
});

//Route Dashboard \\
Route::get('/dasboard.aplikasi','HomeController@index2');
Route::get('filter.tahun','HomeController@index2');
Route::get('/home', 'HomeController@index');

// export excel
Route::get('export_excel', 'excel\ExportExcelController@index');
Route::get('export_excel.cek', 'excel\ExportExcelController@store');
Route::get('export_excelbarang', 'excel\ExportExcelController@index2');
Route::get('export_excel.export', 'excel\ExportExcelController@export');

// Route Barang \\
Route::get('master.barang.grid', 'master\BarangController@barang');
Route::get('master.barang.form', 'master\BarangController@form');
Route::post('master.barang.form', 'master\BarangController@tambah');
Route::get('master.barang.{id_barang}.edit_barang', 'master\BarangController@edit');
Route::post('master.barang.{id_barang}.update', 'master\BarangController@update');
Route::get('master.barang.{id_barang}.grid','master\BarangController@delete');

// Route User \\
Route::get('master.user.grid', 'master\UserController@index');
route::get('master.user.form', 'master\UserController@create');
Route::get('master.user.{id_user}.edit_user', 'master\UserController@edit');
Route::post('/master/user/store', 'master\UserController@store');
Route::get('/master/user/{id_user}/destroy', 'master\UserController@destroy');
Route::post('master/user/{id_user}/update', 'master\UserController@update');
Route::get('master.user.{id_user}.reset','master\UserController@reset');

// Route Inventory Barang Masuk \\
Route::get('inventory.masuk.grid','inventory\masukController@index');
Route::get('inventory.masuk.{id_transaksi}.show','inventory\masukController@show');
Route::get('inventory.masuk.form','inventory\masukController@create');
Route::get('inventory.masuk.{id_transaksi}.edit','inventory\masukController@edit');
Route::post('/inventory/masuk/store', 'inventory\masukController@store');
Route::post('inventory/masuk/{id_transaksi}/update','inventory\masukController@update');
Route::get('/inventory/masuk/{id_transaksi}/destroy','inventory\masukController@destroy');
Route::post('inventory/masuk/{id_transaksi}/permission','inventory\masukController@lock');

// Route Inventory Barang Keluar \\
Route::get('inventory.keluar.grid','inventory\keluarController@index');
Route::get('inventory.keluar.{id_transaksi}.view','inventory\keluarController@show');
Route::get('inventory.keluar.form','inventory\keluarController@create');
Route::get('inventory.keluar.{id_transaksi}.edit', 'inventory\keluarController@edit');
Route::post('/inventory/keluar/store', 'inventory\keluarController@store');
Route::post('inventory/keluar/{id_transaksi}/update', 'inventory\keluarController@update');
Route::get('/inventory/keluar/{id_transaksi}/destroy','inventory\keluarController@destroy');

Route::post('inventory/keluar/{id_transaksi}/permission','inventory\keluarController@kunci');
Route::post('inventory/keluar/{id_transaksi}/buka','inventory\keluarController@buka');
});

